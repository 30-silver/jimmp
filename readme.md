# JIMMP
For GIMP being a graphics editor, it sure hurts to look at. Let's fix it!

JIMMP (judas's improved GIMP) is a theme pack for GIMP that makes it not look bad. If you apply this, GIMP will look so much better and your quality of life will improve by 0.2%...probably.

<img src="https://i.postimg.cc/X3tfDnPg/gimp-theme.png" alt="gimp but looks good" />

## How to install
1. Copy and paste everything from the "GIMP 2" folder into your GIMP installation folder. In Windows, this is ```C:\Program Files\GIMP 2```. Overwrite everything. 

2. (optional) If you wanna use the layout shown in the screenshot, delete your current GIMP app data ```C:\Users\<username>\AppData\Roaming\GIMP```.

Start GIMP and it should work.

## Features
- Darker color scheme with better contrast between colors to make it look more modern.
- New layout that leaves more room for your image.
- New splash screen.

More stuff coming soon!